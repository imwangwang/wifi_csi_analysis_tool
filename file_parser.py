import threading
import time
import struct
import queue
import numpy as np
from parsing_functions import *
from collections import deque

# '<" little endian
# 'I' magic_number
# 'I' vendor_id
# 'B' CFR_header_version
# 'B' CFR_data_version
# 'B' chip_type
# 'B' platform_type
# 'I' CFR_metadata_length
# '8B' host_real_ts
# total 24B

# 'B' capture_status
# 'B' capture_bandwidth
# 'B' channel_bandwidth
# 'B' phy_mode
# 'H' prim20_channel
# 'H' band_center_freq1
# 'H' band_center_freq2
# 'B' capture_mode
# 'B' capture_type
# 'B' STS(Space_Time_Streams) count
# 'B' RX_chain_mask_value
# '8B' timestamp of CRF capture in microseconds
# total 46B

# 'I' CFR_dump_length
# 'B' Whether the received PPDU is MU/SU
# 'B' num_mu_users
# '6B' peer_mac1
# '6B' peer_mac2
# '6B' peer_mac3
# '6B' peer_mac4
# '32B' channel_RSSI (per chain)
# '16B' Phase_per_chain
# 'I' rtt_cfo_measurement
# '8B' agc_gain_per_chain
# 'I' rx_start_ts (Rx packet timestamp)
# 'H' mcs_rate
# 'H' gi_type
# '8B' cfr_su_sig_info
# '8B' agc_gain_tbl_index
class RealTimeMedianFilter:
    def __init__(self, window_size=20, wt = 100.0):
        self.window_size = window_size
        self.data_window = deque(maxlen=window_size)
        self.wt = wt

    def update(self, new_value):
        ret = 0.0
        new_value = np.abs(new_value)
        if len(self.data_window) >= self.window_size:
            variance = np.var(self.data_window)
            print(f"variance:{variance}, new_value:{new_value}")
            if 0.1 < (variance * self.wt):
                ret =  0.4
        self.data_window.append(new_value)
        return ret
            
class CSIFileParser:
    def __init__(self, filename):
        self.filename = filename
        self.should_exit = False
        self.is_paused = False
        self.data_queue = queue.Queue()
        self.cfr_data_hdr_fmt = "<IIBBBBIQBBBBHHHBBBBQIBB6B6B6B6B32B16BI8BIHH8B8B"
        self.cfr_data_hdr_len = 0xA0
        self.cfr_chip_hdr_fmt = "<HHHHHHHH14HI"
        self.expected_header = b'\xAF\xBE\xAD\xDE'#b'\xDE\xAD\xBE\xAF'
        self.thread = threading.Thread(target=self.csi_dump_reader, daemon=True)

        self.cfr_chip_hdr_data = {}
        self.last_prim20_freq = 0
        self.last_center_freq = 0
        self.sequence = 0

        #parser algorithm parameters
        self.memory_mti1 = 0
        self.memory_enh = 0
        self.m = 1
        self.mu_0 = 0.1

        self.median_filter = RealTimeMedianFilter()

        self.old_phase_diffs = np.zeros(54)
        self.old_amplitudes = np.zeros(108)
        self.first_in = True

    def start(self):
        self.thread.start()

    def join(self):
        self.thread.join()

    def stop(self):
        self.should_exit = True
        if self.thread.is_alive():
            self.thread.join()

    def pause(self):
        self.is_paused = not self.is_paused

    def get_middle_bits(self, number, start_bit, num_bits):
        mask = (1 << num_bits) - 1
        mask = mask << start_bit
        middle_bits = (number & mask) >> start_bit
        return middle_bits

    def recvall(self, sock, length):
        data = bytearray()
        while len(data) < length:
            packet = sock.recv(length - len(data))
            if not packet:
                return None
            data.extend(packet)
        return data

    def parse_iq_data(self, tone_iq):
        fft_size = 128
        iq_data = []
        new_tone_iq = []
        num_streams = tone_iq['num_streams']
        num_chains = tone_iq['num_chains']
        upload_pkt_bw = tone_iq['upload_pkt_bw']
        num_tones = 54*(2**upload_pkt_bw)
        tone_iq_dump = tone_iq['iq_d']
        prim20_freq = tone_iq['prim20_freq']
        center_freq = tone_iq['center_freq']
        pri20 = (prim20_freq - center_freq) / 10
        #print('meta=', num_streams,num_chains, upload_pkt_bw, num_tones, pri20)
        
        # iq_data format:
        # ANT0: other chan IQs, prim20 IQ
        # ANT1: other chan IQs, prim20 IQ
        if upload_pkt_bw == 1:
            # 20MHz invalid, 20MHz chain0 prim20 IQ
            # 20MHz invalid, 20MHz chain1 prim20 IQ
            fft_size = 128
            indices1 = slice(54*4, 54*2*4)
            indices2 = slice(54*3*4, 54*4*4)
            new_tone_iq = tone_iq_dump[indices1] + tone_iq_dump[indices2]
        elif upload_pkt_bw == 2:
            # 60MHz invalid, 20MHz chain0 prim20 IQ
            # 60MHz invalid, 20MHz chain1 prim20 IQ
            fft_size = 256
            indices1 = slice(54*3*4, 54*4*4)
            indices2 = slice(54*7*4, 54*8*4)
            new_tone_iq = tone_iq_dump[indices1] + tone_iq_dump[indices2]
        elif upload_pkt_bw == 3:
            # 140MHz invalid, 20MHz chain0 prim20 IQ
            # 140MHz invalid, 20MHz chain1 prim20 IQ
            fft_size = 512
            indices1 = slice(54*7*4, 54*8*4)
            indices2 = slice(54*15*4, 54*16*4)
            new_tone_iq = tone_iq_dump[indices1] + tone_iq_dump[indices2]
        else:
            new_tone_iq = tone_iq_dump
        # print('num_tones=', num_tones, 'length of tone_iq_dump=', 
        #      len(tone_iq_dump), 'length of new_tone_iq=', len(new_tone_iq))

        for i in range(0, len(new_tone_iq), 8):
            word = new_tone_iq[i:i+8]

            tone1 = struct.unpack('<I', word[:4])[0]
            tone0 = struct.unpack('<I', word[4:])[0]
            #print ('tone1=', hex(tone1))
            #print ('tone0=', hex(tone0))
            tone1q = (tone1 >> 16) & 0xFFFF
            if ( tone1q > (2**15-1)):
                tone1q = tone1q - 65536
            tone1i = (tone1 & 0xFFFF) 
            if (tone1i > (2**15-1)):
                tone1i = tone1i - 65536
        
            tone0q = (tone0 >> 16) & 0xFFFF
            if ( tone0q > (2**15-1)):
                tone0q = tone0q - 65536
            tone0i = (tone0 & 0xFFFF) 
            if (tone0i > (2**15-1)):
                tone0i = tone0i - 65536
            
            iq_data.append([tone0i, tone0q])
            iq_data.append([tone1i, tone1q])
        
        return np.array(iq_data)
    
    #def iq_to_antenna_offset(self, iq_in):   
    def subtract_antenna_arrays(self, iq_data):
        # Convert the list of IQ pairs into a structured complex array
        num_tones = 54  # As per your specifications that there are 54 tones per antenna
        if len(iq_data) < num_tones * 2:  # Check to ensure there are enough data points
            raise ValueError("Insufficient data to process both antennas")

        # Reshape the IQ data into an array of complex numbers
        iq_complex = [complex(iq[0], iq[1]) for iq in iq_data]

        # Separate the complex numbers into two groups: Antenna 1 and Antenna 2
        ant1_complex = np.array(iq_complex[:num_tones])
        ant2_complex = np.array(iq_complex[num_tones:num_tones * 2])

        # Compute the difference between corresponding elements of Antenna 1 and Antenna 2
        result_complex = ant1_complex - ant2_complex

        return result_complex
    
    def iq_to_amplitude_phase2(self, iq_in):
        result = self.subtract_antenna_arrays(iq_in)
        num = iq_in.shape[0]
        num2 = len(result)
        amplitude_out = np.zeros(num)
        phase_out = np.zeros(num)

        for ii in range(num2):
            amplitude_out[ii] = np.abs(result[ii])
            phase_out[ii] = np.arctan2(result[ii].imag, result[ii].real) / np.pi
        
        amplitude = amplitude_out[3]
        diff_subcarrier_2 = phase_out[3]
        diff_subcarrier_4 = 0 #phase_out[4] - phase_out[4+54]
        diff_subcarrier_8 = 0 #phase_out[8] - phase_out[8+54]

        return amplitude_out, phase_out, diff_subcarrier_2, diff_subcarrier_4, diff_subcarrier_8, amplitude


    def iq_to_amplitude_phase(self, iq_in):
        # Calculate amplitudes directly from the iq_in array
        amplitude_out = np.sqrt(np.sum(iq_in**2, axis=1))

        # Calculate phases using vectorized np.arctan2 for the whole array
        phase_out = np.arctan2(iq_in[:, 1], iq_in[:, 0])

        # Replace NaN values in phase_out with zero
        phase_out = np.nan_to_num(phase_out)  # This replaces NaNs with 0 and can also handle infinities if needed
    
        def wrap_phase(phase):
            return ((phase + np.pi) % (2 * np.pi) - np.pi) / np.pi

        # Calculate phase differences for subcarriers [0 to 53] - [54 to 107]
        phase_diffs = phase_out[:54] - phase_out[54:108]

        amplitude_diffs = amplitude_out[:54] - amplitude_out[54:108]
        amplitude_diffs = np.abs(amplitude_diffs)
        #diff_amplitude = np.mean(amplitude_diffs)

        amplitude_all_diffs = np.abs(amplitude_out - self.old_amplitudes)
        amplitude_high = (amplitude_all_diffs > 1000.) + np.zeros(amplitude_all_diffs.shape)
        diff_amplitude = np.mean(amplitude_all_diffs)

        phase_diffs = wrap_phase(phase_diffs)
        diff_subcarrier = np.mean(np.abs(phase_diffs)) #phase_diffs[3]

        raw_diff_subcarrier = diff_subcarrier

        amplitude_sum = 0
        if self.first_in:
            self.first_in = False
        else:
            amplitude_sum = np.sum(amplitude_high)
            #sign_diffs = np.sign(phase_diffs) != np.sign(self.old_phase_diffs)
            #sign_flip = np.sum(sign_diffs)

        print(f"amplitude_sum:{amplitude_sum}")

        if amplitude_sum > 2:
            self.m = 1
            self.memory_mti1 = 0.0
            diff_subcarrier = 0
            #print(f"phase_diffs now: {phase_diffs}, \nthen: {self.old_phase_diffs}")
            print(f"amplitude all diffs: {amplitude_all_diffs}")
        else:
            #diff_subcarrier = np.abs(raw_diff_subcarrier)
            diff_subcarrier, self.memory_mti1 = clutter_removal_mti(diff_subcarrier, self.memory_mti1, self.m, mu_0=self.mu_0)
            self.m += 1

        #print(f"phase_diff now: {phase_diffs[2]}, then: {self.raw_diff_subcarrier_bk}")
        self.old_phase_diffs = phase_diffs
        self.old_amplitudes = amplitude_out

        move_detection = self.median_filter.update(diff_subcarrier)
        #diff_subcarrier_2, self.memory_enh = enhancement(diff_subcarrier_2, self.memory_enh, rho_0=0.05, skip_square=0, skip_filter=1)

        print("filter_phase_diff_subcariier=", diff_subcarrier, ", raw_phase_diff_subcarrier_2=", raw_diff_subcarrier, ", detection:", move_detection)
        return amplitude_out, phase_out, amplitude_diffs, phase_diffs, diff_amplitude, diff_subcarrier, move_detection

    def csi_dump_reader(self):
        try:
            with open(self.filename, 'rb') as file:
                while not self.should_exit:
                    if self.is_paused:
                        time.sleep(0.1)
                        continue
                    # Your file reading and data processing logic here
                    processed_data = self.parse_data(file)
                    if processed_data:
                        self.data_queue.put(processed_data)
                        print(f"Processed data sequence: {self.sequence}")
                        time.sleep(0.1)
                    else:
                        break
        except FileNotFoundError:
            print("File not found")
        except Exception as e:
            print(f"Error processing file: {e}")

    def parse_data(self, file):
        cfr_data_hdr = {}
        data_body1 = file.read(self.cfr_data_hdr_len)
        if len(data_body1) != self.cfr_data_hdr_len:
            print(f"can not read data header:{len(data_body1)}, maybe end of file?")
            return None
        if data_body1[:4] != self.expected_header:
            #print(data_body1)
            print("invalid header")
            return None
        #print("got binary data length:%d"%len(binary_data1))
        data = struct.unpack(self.cfr_data_hdr_fmt, data_body1)
        cfr_data_hdr["magic_number"] =              data[0]
        cfr_data_hdr["vendor_id"] =                 data[1]
        cfr_data_hdr["CFR_header_version"] =        data[2]
        cfr_data_hdr["CFR_data_version"] =          data[3]
        cfr_data_hdr["chip_type"]                 = data[4]
        cfr_data_hdr["platform_type"] =             data[5]
        cfr_data_hdr["CFR_metadata_length"] =       data[6]
        cfr_data_hdr["host_real_ts"] =              data[7]
        cfr_data_hdr["capture_status"] =            data[8]
        cfr_data_hdr["capture_bandwidth"] =         data[9]
        cfr_data_hdr["channel_bandwidth"] =         data[10]
        cfr_data_hdr["phy_mode"] =                  data[11]
        cfr_data_hdr["prim20_channel"] =            data[12]
        cfr_data_hdr["band_center_freq1"] =         data[13]
        cfr_data_hdr["band_center_freq2"] =         data[14]
        cfr_data_hdr["capture_mode"] =              data[15]
        cfr_data_hdr["capture_type"] =              data[16]
        cfr_data_hdr["Space_Time_Streams_count"] =  data[17]
        cfr_data_hdr["RX_chain_mask_value"]      =  data[18]
        cfr_data_hdr["timestamp_CRF"]       =       data[19]
        cfr_data_hdr["CFR_dump_length"] =           data[20]
        cfr_data_hdr["is_MU_SU"] =                  data[21]
        cfr_data_hdr["num_mu_users"] =              data[22]
        cfr_data_hdr["peer_mac1"] =                 data[23:29]
        cfr_data_hdr["peer_mac2"] =                 data[29:35]
        cfr_data_hdr["peer_mac3"] =                 data[35:41]
        cfr_data_hdr["peer_mac4"] =                 data[41:47]
        cfr_data_hdr["channel_RSSI"] =              data[47:79]
        cfr_data_hdr["Phase_per_chain"] =           data[79:95]
        cfr_data_hdr["rtt_cfo_measurement"] =       data[95]
        cfr_data_hdr["agc_gain_per_chain"] =        data[96:105]
        cfr_data_hdr["rx_start_ts"] =               data[105]
        cfr_data_hdr["mcs_rate"] =                  data[106]
        cfr_data_hdr["gi_type"] =                   data[107]
        cfr_data_hdr["cfr_su_sig_info"] =           data[108:116]
        cfr_data_hdr["agc_gain_tbl_index"] =        data[116:124]
        
        #print(cfr_data_hdr)

        data_body_len = cfr_data_hdr["CFR_dump_length"]
        #print("got data body len:%d"%data_body_len)
        data_body2 = file.read(48)
        if len(data_body2) != 48:
            print(f"can not read data header:{len(data_body2)}, maybe end of file?")
            return None

        data2 = struct.unpack(self.cfr_chip_hdr_fmt, data_body2)

        self.cfr_chip_hdr_data["rtt_tag"] = self.get_middle_bits(data2[0], 0, 8)
        self.cfr_chip_hdr_data["header_length"] = self.get_middle_bits(data2[0], 8, 6)

        self.cfr_chip_hdr_data["upload_done"] = self.get_middle_bits(data2[1], 0, 1)             # 1bit
        self.cfr_chip_hdr_data["capture_type"] = self.get_middle_bits(data2[1], 1, 3)     # 3bits
        self.cfr_chip_hdr_data["preamble_type"] = self.get_middle_bits(data2[1], 4, 2)    # 2bits
        self.cfr_chip_hdr_data["nss"] = self.get_middle_bits(data2[1], 6, 3)              # 3bits
        self.cfr_chip_hdr_data["num_chains"] = self.get_middle_bits(data2[1], 9, 3)       # 3bits
        self.cfr_chip_hdr_data["upload_pkt_bw"] = self.get_middle_bits(data2[1], 12, 3)   # 3bits
        self.cfr_chip_hdr_data["sw_peer_id_valid"] = self.get_middle_bits(data2[1], 15, 1)# 1bit

        self.cfr_chip_hdr_data["sw_peer_id"] = data2[2]

        self.cfr_chip_hdr_data["phy_ppdu_id"] = data2[3]

        self.cfr_chip_hdr_data["total_bytes"] = data2[4]

        self.cfr_chip_hdr_data["header_version"] = self.get_middle_bits(data2[5], 0, 4)        # 4bits
        self.cfr_chip_hdr_data["target_id"] = self.get_middle_bits(data2[5], 4, 4)     # 4bits
        self.cfr_chip_hdr_data["cfr_fmt"] = self.get_middle_bits(data2[5], 8, 1)      # 1bit
        self.cfr_chip_hdr_data["mu_rx_data_incl"] = self.get_middle_bits(data2[5], 9, 1) # 1bit
        self.cfr_chip_hdr_data["freeze_data_incl"] = self.get_middle_bits(data2[5], 10, 1) # 1bit

        self.cfr_chip_hdr_data["mu_rx_num_users"] = self.get_middle_bits(data2[6], 0, 8)
        self.cfr_chip_hdr_data["decimation_factor"] = self.get_middle_bits(data2[6], 8, 4)

        #print(self.cfr_chip_hdr_data)
            
        data_body_len -= 48 # skip chip header
        data_body_len += 4 # add tail mark
        data_body3 = file.read(data_body_len)
        if len(data_body3) != data_body_len:
            print(f"can not read data header:{len(data_body3)}, maybe end of file?")
            return None
                        
        iq_data = {
        'prim20_freq': cfr_data_hdr["prim20_channel"],
        'center_freq': cfr_data_hdr["band_center_freq1"],
        'num_streams': self.cfr_chip_hdr_data["nss"] + 1,
        'num_chains': self.cfr_chip_hdr_data["num_chains"],
        'upload_pkt_bw': self.cfr_chip_hdr_data["upload_pkt_bw"],
        'iq_d': data_body3[:-4],
        'tag': self.sequence
        }

        #print(iq_data["iq_d"])
        if iq_data["prim20_freq"] != self.last_prim20_freq:
            print("prim20 freq:%d"%iq_data["prim20_freq"])
            self.last_prim20_freq = iq_data["prim20_freq"]
        
        if iq_data["center_freq"] != self.last_center_freq:
            print("center freq:%d"%iq_data["center_freq"])
            self.last_center_freq = iq_data["center_freq"]
        
        self.sequence += 1
        
        iq_dataout = self.parse_iq_data(iq_data)
        amp_data, phase_data, diff_amplitues, diff_phases, amplitude_average, phase_average, detection = self.iq_to_amplitude_phase(iq_dataout)
        self.processed_data = (amp_data, phase_data, diff_amplitues, diff_phases, amplitude_average, phase_average, detection)
        return self.processed_data  # Placeholder for actual parsed data

    def get_data_from_queue(self):
        if not self.data_queue.empty():
            return self.data_queue.get()
        return None
