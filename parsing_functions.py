#Copyright (c) 2022 Qorvo, Inc
#All rights reserved.
#NOTICE: All information contained herein is, and remains the property
#of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
#concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
#may be covered by patents, patent applications, and are protected by
#trade secret and/or copyright law. Dissemination of this information
#or reproduction of this material is strictly forbidden unless prior written
#permission is obtained from Qorvo, Inc.

import numpy as np
from sklearn.cluster import DBSCAN
from scipy.optimize import linear_sum_assignment
import random


def clutter_removal_mti(acc, memory_in, m, mu_0):
    """
    clutter_removal_mti estimates and removes the constant background clutter

    :param acc         : CIR at slowtime index m
    :param memory_in   : clutter estimation at slowtime index m-1
    :param m           : slowtime index
    :param mu_0        : minimum learning rate
    :return x          : CIR at slowtime index m after clutter removal (MTI)
    :return memory_out : clutter estimation at slowtime index m
    """
    mu = max(mu_0, 1/m)
    memory_out = memory_in + mu * (acc - memory_in)
    x = acc - memory_out

    return x, memory_out


def enhancement(x, memory_in, rho_0, skip_square, skip_filter):
    """
    enhancement takes the abs of the input and optionally squares it and/or applies 
    a filtering operation over the slowtime

    :param x           : CIR at slowtime index m after clutter removal (MTI)
    :param memory_in   : enhanced MTI at slowtime index m-1
    :param rho_0       : filtering factor
    :param skip_square : flag to skip squaring operation
    :param skip_filter : flag to skip filtering operation
    :return p          : enhanced MTI at slowtime index m
    :return memory_out : enhanced MTI at slowtime index m
    """
    y = np.abs(x)
    if skip_square:
        z = np.copy(y)
    else:
        z = np.sign(x) * np.power(y, 0.5)
    if skip_filter:
        memory_out = memory_in
        p = np.copy(z)
    else:
        memory_out = memory_in + rho_0 * (z - memory_in)
        p = memory_out

    return p, memory_out


def noise_whitening(p, p_0):
    """
    noise_whitening compensates for the noise level mismatches before the detection

    :param p   : enhanced MTI 
    :param p_0 : noise levels corresponding to all the taps.
    :return q  : enhanced MTI after noise whitening
    """
    if p_0 is None:
        q = np.copy(p)
    else:
        q = np.divide(p, p_0, out=np.zeros_like(p), where=p_0 != 0)

    return q


def detection(q, w_t, kst, ksp):
    """
    detection applies a simplified version of the CFAR

    :param q   : enhanced MTI after noise whitening
    :param w_t : factor used to adjust the threshold level
    :param kst : first index used in threshold calculation
    :param ksp : last index used in threshold calculation
    :return b  : CFAR output (binary array)
    """
    t = w_t * ((np.sum(q[kst-1:ksp+1])) / (ksp-kst+1))
    b = (q > t) + np.zeros(q.shape)

    return b


def clustering(b, minpts, eps, n_c_max):
    """
    clustering groups all the fasttime indices that corresponds to the same target

    :param b       : CFAR output
    :param minpts  : number of points in a neighborhood for a point to be 
                     considered as a core point
    :param eps     : maximum distance between two points for one to be 
                     considered as in the neighborhood of the other
    :param n_c_max : maximum number of clusters (if we found more then 
                     the clusters with the smallest indices are kept)
    :return c      : clustering output, c[i]=0 -> i is not a cluster,  
                     c[i]=j in {1, ..., n_c_max} -> i in cluster j
    """
    clust_algo = DBSCAN(eps=eps, min_samples=minpts)
    c = np.copy(b)
    nonzero_taps = np.nonzero(b)[0]
    if nonzero_taps.size != 0:
        clust_result = clust_algo.fit_predict(nonzero_taps.reshape(-1, 1))
        clust_result[clust_result > n_c_max-1] = -1
        c[nonzero_taps] = 1 + clust_result

    return c


def distance_estimation(c, d_0):
    """
    distance_estimation calculates the distance of each cluster

    :param c   : clustering output
    :param d_0 : zero-distance offset
    :return d  : distance clustering output, d[i]=np.nan i is not a cluster, 
                  d[i]=j  -> cluster i is associated with distance j
    """
    # TODO change distance to the one maximising the signal
    sampling_rate = 1e+9
    speed_of_light = 3e+8
    d = np.copy(c)
    d[d == 0] = np.nan
    clusters = np.unique(c[c != 0])
    for cluster in clusters:
        idxs = np.where(c == cluster)[0]
        d[idxs] = ((1/sampling_rate) * np.min(idxs)
                   * (speed_of_light / 2)) - d_0

    return d


def pdoa_estimation(old_x2, old_x1, x1):
    """
    pdoa_estimation estimates the phase difference of arrival

    :param old_x2 : MTI from second antenna at slowtime index m-1
    :param old_x1 : MTI from first antenna at slowtime index m-1
    :param x1     : MTI from first antenna at slowtime index m
    :return pdoa  : the two pdoa estimations
    """
    pdoa = np.empty((2, 64))
    pdoa[0, :] = np.angle(old_x1 * old_x2.conj())
    pdoa[1, :] = np.angle(x1 * old_x2.conj())

    return pdoa


def peak_based_pdoa_estimation(q, c, pdoa, a_0):
    """
    peak_based_pdoa_estimation selects the pdoa angle that corresponds to the fasttime 
    index of the cluster's point having the strongest level.

    :param q    : enhanced MTI after noise whitening
    :param c    : clustering output
    :param pdoa : the two pdoa estimations
    :param a_0  : zero-angle offset
    :return a   : pdoa clustering output, a[:, i]=[np.nan, np.nan] -> i is not a cluster,
                  a[:, i]=[j1, j2] -> cluster i is associated with pdoa [j1, j2]
    """
    a = np.empty((2, c.size))
    a[0, :] = np.copy(c)
    a[1, :] = np.copy(c)
    a[a == 0] = np.nan
    clusters = np.unique(c[c != 0])
    for cluster in clusters:
        idxs = np.where(c == cluster)[0]
        k_star = np.argmax(q[idxs]) + idxs[0]
        a[0, idxs] = pdoa[0, k_star] - a_0
        a[1, idxs] = pdoa[1, k_star] - a_0
    # put the phase in [-np.pi, np.pi]
    a = np.mod(a+np.pi, 2*np.pi) - np.pi

    return a


class Track:
    def __init__(self):
        # each track has a status, it can be either 'confirmed' or 'attempt'
        self.status = ''

        # age of track to determine its status evolution
        #self.age4t2d = None
        #self.age4t2c = None
        #self.age4c2d = None

        # number of time the tracks have been assigned an observation
        self.assigned4t2d = None
        self.assigned4t2c = None
        self.assigned4c2d = None

        # current distance estimation
        self.distance = None

        # current speed estimation
        self.speed = None

        # current pdoa estimation from odd antenna switch interval
        self.pdoa1 = None

        # current pdoa estimation from even antenna switch interval
        self.pdoa2 = None

        # it contains the limit of valid range for the next observation
        self.gate = (None, None)

    def __str__(self):
        return "Track % s status:% s distance:% s speed: % s gate: % s pdoa1 % s pdoa2 % s assigned4t2d % s age4t2d % s assigned4t2c % s age4t2c % s assigned4c2d %s age4c2d %s" % (id(self), self.status, self.distance, self.speed, self.gate, self.pdoa1, self.pdoa2, self.assigned4t2d, self.age4t2d, self.assigned4t2c, self.age4t2c, self.assigned4c2d, self.age4c2d)

    def maintain_attempt_track(self, m4t2d, n4t2d, m4t2c, n4t2c):
        """
        maintain_attempt_track maintains an attempt track, which can be either deleted or confirmed
        :param m4t2d, n4t2d : an attempt track is deleted if it has not been assigned at least m4t2d times during the
                              last n4t2d slow times
        :param m4t2c, n4t2c : an attempt track is confirmed if it has been assigned for at least m4t2c times during the 
                              last n4t2c slow times.
        :return             : True if the track is kept, False if the track is to be deleted and in case the track is 
                              confirmed, is status is changed
        """
        if len(self.assigned4t2d) >= n4t2d:
            if self.assigned4t2d.count(1) < m4t2d:
                # kill the track
                return False
            else :
                # you're fine for now
                self.assigned4t2d = self.assigned4t2d[1:]

        if self.assigned4t2c.count(1) >= m4t2c:
            # promote the track
            self.status = 'confirmed'

        return True

    def maintain_confirmed_track(self, m4c2d, n4c2d):
        """
        maintain_confirmed_track maintains a confirmed track, which can be either deleted or kept confirmed
        :param m4c2d, n4c2d : a confirmed track is deleted if the track has not been assigned at least m4c2d times during the
                              last n4c2d slow times
        :return             : True if the track is kept, False if the track is to be deleted
        """
        
        if len(self.assigned4c2d) >= n4c2d:
            if self.assigned4c2d.count(1) < m4c2d:
                # kill the track
                return False
            else:
                # you're fine for now
                self.assigned4c2d = self.assigned4c2d[1:]
        return True

    def maintain_track(self, m4t2d, n4t2d, m4t2c, n4t2c, m4c2d, n4c2d):
        """
        maintain_track a wrapper function to maintain the track
        :return : True if the track is to be kept, False if not
        """
        if self.status == 'attempt':
            keep = self.maintain_attempt_track(m4t2d, n4t2d, m4t2c, n4t2c)

        elif self.status == 'confirmed':
            keep = self.maintain_confirmed_track(m4c2d, n4c2d)

        return keep

    def filter_assigned(self, d_l, a1_l, a2_l, sigma, delta, alpha):
        """
        filter_assigned filter a track that have been assigned an observation
        :param d_l   : the assigned distance
        :param a1_l  : the assigned pdoa estimation from odd antenna switch interval
        :param a2_l  : the assigned pdoa estimation from even antenna switch interval
        :param sigma : speed tracking convergence coefficient 
        :param delta : distance tracking convergence coefficient 
        :param alpha : pdoa tracking convergence coefficient 
        """
        # error estimation
        e = d_l - self.distance

        # speed update
        self.speed = self.speed + sigma * delta * e

        # distance update
        self.distance = self.distance + self.speed + sigma * e

        # angle correction
        kappa1, kappa2 = 0, 0
        a1_l_bar = a1_l + kappa1*self.speed
        a2_l_bar = a2_l - kappa2*self.speed

        # angle unwrapping
        if a1_l_bar - self.pdoa1 > 1*np.pi:
            a1_l_bar = a1_l_bar - 2*np.pi
        if a1_l_bar - self.pdoa1 < -1*np.pi:
            a1_l_bar = a1_l_bar + 2*np.pi

        if a2_l_bar - self.pdoa2 > 1*np.pi:
            a2_l_bar = a2_l_bar - 2*np.pi
        if a2_l_bar - self.pdoa2 < -1*np.pi:
            a2_l_bar = a2_l_bar + 2*np.pi

        # angle update
        self.pdoa1 = self.pdoa1 + alpha*(a1_l_bar - self.pdoa1)
        self.pdoa2 = self.pdoa2 + alpha*(a2_l_bar - self.pdoa2)

        # the second estimation is unwrapped relatively to the first one
        if self.pdoa2 - self.pdoa1 > np.pi:
            self.pdoa2 = self.pdoa2 - 2*np.pi

        if self.pdoa2 - self.pdoa1 < -np.pi:
            self.pdoa2 = self.pdoa2 + 2*np.pi

    def filter_not_assigned(self):
        """
        filter_not_assigned filter a track that have not been assigned an observation
        """
        self.distance = self.distance + self.speed

    def update_gate(self, o_m, o_M):
        """
        update_gate updates the gate
        :param o_m   : user defined parameters for gate update
        :param o_M   : user defined parameters for gate update
        """
        self.gate = (self.distance + self.speed - o_m,
                     self.distance + self.speed + o_M)

    def update_assigned_history(self, assigned):
        if assigned:
            if self.status == 'attempt':
                self.assigned4t2c.append(1)
                self.assigned4t2d.append(1)
            elif self.status == 'confirmed':
                self.assigned4c2d.append(1)
        else :
            if self.status == 'attempt':
                self.assigned4t2c.append(0)
                self.assigned4t2d.append(0)
            elif self.status == 'confirmed':
                self.assigned4c2d.append(0)        

def observation_to_track_association(track_list, d, a):
    """
    observation_to_track_association checks if new observations (d, a) can be assigned to the current living tracks list
    :param track_list     : living tracks list
    :param d              : distance clustering output
    :param a              : pdoa clustering output
    :return assign_output : assign_output[id] = (d_l, a1_l, a2_l) to indicate that track with id(track)=id is to be assigned 
                            (d_l, a1_l, a2_l), assign_output[id] = None to if no observation was assigned to track with id(track)=id
    """
    # Fill a cost matrix 56 of dimension (n_t, n_c)
    distances = (np.unique(d[~np.isnan(d)]))
    n_c = distances.size
    n_t = len(track_list)
    C = np.empty((n_t, n_c))
    inf_cost = 1e+9
    for u, t in enumerate(track_list):
        for i, distance in enumerate(distances):
            if distance > t.gate[0] and distance < t.gate[1]:
                C[u, i] = np.abs(distance - t.distance) ** 2
            else:
                C[u, i] = inf_cost

    # Assign observations to tracks applying the Munkes (Hungarian) algorithm
    assign_output = dict.fromkeys(map(lambda t: id(t), track_list))

    t_idx, d_idx = linear_sum_assignment(C)

    for ind, d_ind in enumerate(d_idx):
        if C[t_idx[ind], d_ind] < inf_cost:
            # assign observation distances[d_ind] to track track_list[t_idx[ind]]
            # assign observation (a[0, np.where(d == distances[d_ind])][0], a[1, np.where(d == distances[d_ind])][0]) to track track_list[t_idx[ind]]
            d_l = distances[d_ind]
            a1_l = a[0, np.where(d == d_l)][0][0]
            a2_l = a[1, np.where(d == d_l)][0][0]

            assign_output[id(track_list[t_idx[ind]])] = (d_l, a1_l, a2_l)
            # update assigned counter of track_list[t_idx[ind]]
            #track_list[t_idx[ind]].assigned4t2d += 1
            #track_list[t_idx[ind]].assigned4t2c += 1
            #track_list[t_idx[ind]].assigned4c2d += 1

    # Deal with the case where we have more observations than tracks
    for i, distance in enumerate(distances):
        if i not in d_idx:
            d_l = distance
            a1_l = a[0, np.where(d == d_l)][0][0]
            a2_l = a[1, np.where(d == d_l)][0][0]

            new_track = track_initialization(d_l, a1_l, a2_l)
            track_list.append(new_track)
            assign_output[id(new_track)] = (d_l, a1_l, a2_l)

    return assign_output


def track_initialization(d_l, a1_l, a2_l):
    """
    track_initialization create a new track using observations (d_l, a1_l, a2_l)
    :param d_l           : the assigned distance
    :param a1_l          : the assigned pdoa estimation from odd antenna switch interval
    :param a2_l          : the assigned pdoa estimation from even antenna switch interval
    :return new_track    : new track
    """
    new_track = Track()
    new_track.status = 'attempt'
    new_track.distance = d_l
    new_track.speed = 0

    new_track.pdoa1 = a1_l
    new_track.pdoa2 = a2_l

    # the second estimation is unwrapped relatively to the first one
    if new_track.pdoa2 - new_track.pdoa1 > np.pi:
        new_track.pdoa2 = new_track.pdoa2 - 2*np.pi

    if new_track.pdoa2 - new_track.pdoa1 < -np.pi:
        new_track.pdoa2 = new_track.pdoa2 + 2*np.pi

    new_track.gate = (-np.inf, np.inf)
    #new_track.age4t2d = 0
    #new_track.age4t2c = 0
    #new_track.age4c2d = 0
    new_track.assigned4t2d = []
    new_track.assigned4t2c = []
    new_track.assigned4c2d = []

    return new_track


def pdoas2aoa(pdoa1, pdoa2):
    pdoa_mean = (pdoa1 + pdoa2)/2
    pdoa_mean = np.mod(pdoa_mean+np.pi, 2*np.pi) - np.pi
    pdoa_mean = pdoa_mean / (2 * np.pi * 0.48)
    pdoa_mean = np.clip(pdoa_mean, -1, 1)
    aoa = np.arcsin(-pdoa_mean)
    return aoa


def pdoas2position(pdoa1, pdoa2, distance):
    pdoa_mean = (pdoa1 + pdoa2)/2
    pdoa_mean = np.mod(pdoa_mean+np.pi, 2*np.pi) - np.pi
    pdoa_mean = pdoa_mean / (2 * np.pi * 0.48)
    pdoa_mean = np.clip(pdoa_mean, -1, 1)

    x = distance * pdoa_mean
    y = np.sqrt(np.power(distance, 2) - np.power(x, 2))

    return (x, y)