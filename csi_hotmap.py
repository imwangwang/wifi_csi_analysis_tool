import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.colors import Normalize

import threading
import queue
import time
import socket
import struct

total_numof_signals = 1
total_numof_samples = 1000

cfr_data_hdr_fmt = "<IIBBBBIQBBBBHHHBBBBQIBB6B6B6B6B32B16BI8BIHH8B8B"
cfr_data_hdr = {}
# '<" little endian
# 'I' magic_number
# 'I' vendor_id
# 'B' CFR_header_version
# 'B' CFR_data_version
# 'B' chip_type
# 'B' platform_type
# 'I' CFR_metadata_length
# '8B' host_real_ts
# total 24B

# 'B' capture_status
# 'B' capture_bandwidth
# 'B' channel_bandwidth
# 'B' phy_mode
# 'H' prim20_channel
# 'H' band_center_freq1
# 'H' band_center_freq2
# 'B' capture_mode
# 'B' capture_type
# 'B' STS(Space_Time_Streams) count
# 'B' RX_chain_mask_value
# '8B' timestamp of CRF capture in microseconds
# total 46B

# 'I' CFR_dump_length
# 'B' Whether the received PPDU is MU/SU
# 'B' num_mu_users
# '6B' peer_mac1
# '6B' peer_mac2
# '6B' peer_mac3
# '6B' peer_mac4
# '32B' channel_RSSI (per chain)
# '16B' Phase_per_chain
# 'I' rtt_cfo_measurement
# '8B' agc_gain_per_chain
# 'I' rx_start_ts (Rx packet timestamp)
# 'H' mcs_rate
# 'H' gi_type
# '8B' cfr_su_sig_info
# '8B' agc_gain_tbl_index
cfr_data_hdr_len = 0xA0

cfr_chip_hdr_fmt = "<HHHHHHHH14HI"
cfr_chip_hdr_data = {}

num_freq = 108
buffer_length = 1000  # 环形缓冲区长度
sample_rate = 10  # 每秒10个样本

# 队列初始化
data_queue = queue.Queue()

fig, ax = plt.subplots()
norm = Normalize(vmin=0, vmax=3000)  # 归一化范围

time = np.linspace(0, buffer_length / sample_rate, buffer_length)
freq = np.linspace(1, 108, num_freq)
data_matrix = np.zeros((num_freq, buffer_length))
pcm = ax.pcolormesh(time, freq, data_matrix, cmap='viridis', norm=norm, shading='auto')
fig.colorbar(pcm)
ax.set_xlabel('Time (seconds)')
ax.set_ylabel('Frequency')
ax.set_title('Dynamic Heatmap of WiFi CSI Data')

def get_middle_bits(number, start_bit, num_bits):
    mask = (1 << num_bits) - 1
    mask = mask << start_bit
    middle_bits = (number & mask) >> start_bit
    return middle_bits

def recvall(sock, length):
    data = bytearray()
    while len(data) < length:
        packet = sock.recv(length - len(data))
        if not packet:
            return None
        data.extend(packet)
    return data

def parse_iq_data(tone_iq):
    fft_size = 128
    iq_data = []
    new_tone_iq = []
    num_streams = tone_iq['num_streams']
    num_chains = tone_iq['num_chains']
    upload_pkt_bw = tone_iq['upload_pkt_bw']
    num_tones = 54*(2**upload_pkt_bw)
    tone_iq_dump = tone_iq['iq_d']
    prim20_freq = tone_iq['prim20_freq']
    center_freq = tone_iq['center_freq']
    pri20 = (prim20_freq - center_freq) / 10
    #print('meta=', num_streams,num_chains, upload_pkt_bw, num_tones, pri20)
    
    # iq_data format:
    # ANT0: other chan IQs, prim20 IQ
    # ANT1: other chan IQs, prim20 IQ
    if upload_pkt_bw == 1:
        # 20MHz invalid, 20MHz chain0 prim20 IQ
        # 20MHz invalid, 20MHz chain1 prim20 IQ
        fft_size = 128
        indices1 = slice(54*4, 54*2*4)
        indices2 = slice(54*3*4, 54*4*4)
        new_tone_iq = tone_iq_dump[indices1] + tone_iq_dump[indices2]
    elif upload_pkt_bw == 2:
        # 60MHz invalid, 20MHz chain0 prim20 IQ
        # 60MHz invalid, 20MHz chain1 prim20 IQ
        fft_size = 256
        indices1 = slice(54*3*4, 54*4*4)
        indices2 = slice(54*7*4, 54*8*4)
        new_tone_iq = tone_iq_dump[indices1] + tone_iq_dump[indices2]
    elif upload_pkt_bw == 3:
        # 140MHz invalid, 20MHz chain0 prim20 IQ
        # 140MHz invalid, 20MHz chain1 prim20 IQ
        fft_size = 512
        indices1 = slice(54*7*4, 54*8*4)
        indices2 = slice(54*15*4, 54*16*4)
        new_tone_iq = tone_iq_dump[indices1] + tone_iq_dump[indices2]
    else:
        new_tone_iq = tone_iq_dump
    # print('num_tones=', num_tones, 'length of tone_iq_dump=', 
    #      len(tone_iq_dump), 'length of new_tone_iq=', len(new_tone_iq))

    for i in range(0, len(new_tone_iq), 8):
        word = new_tone_iq[i:i+8]

        tone1 = struct.unpack('<I', word[:4])[0]
        tone0 = struct.unpack('<I', word[4:])[0]
        #print ('tone1=', hex(tone1))
        #print ('tone0=', hex(tone0))
        tone1q = (tone1 >> 16) & 0xFFFF
        if ( tone1q > (2**15-1)):
            tone1q = tone1q - 65536
        tone1i = (tone1 & 0xFFFF) 
        if (tone1i > (2**15-1)):
            tone1i = tone1i - 65536
	
        tone0q = (tone0 >> 16) & 0xFFFF
        if ( tone0q > (2**15-1)):
            tone0q = tone0q - 65536
        tone0i = (tone0 & 0xFFFF) 
        if (tone0i > (2**15-1)):
            tone0i = tone0i - 65536
        
        iq_data.append([tone0i, tone0q])
        iq_data.append([tone1i, tone1q])
    
    return np.array(iq_data)

def iq_to_amplitude_phase(iq_in):
    num = iq_in.shape[0]
    amplitude_phase_out = np.zeros(num)
    for ii in range(num):
        m  = np.sqrt(iq_in[ii, 0]**2 + iq_in[ii, 1]**2)
        amplitude_phase_out[ii]  = m

    return amplitude_phase_out

def tcp_listener():
    global data_queue
    last_prim20_freq = 0
    last_center_freq = 0
    sequence = 0
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(('localhost', 5555))
        #s.listen()
        #conn, addr = s.accept()
        conn = s
        with conn:
            print('Connected')
            while True:
                cfr_data_hdr = {}
                #binary_data1 = conn.recv(cfr_data_hdr_len)
                binary_data1 = recvall(conn, cfr_data_hdr_len)
                if not binary_data1:
                    break
                #print("got binary data length:%d"%len(binary_data1))
                data = struct.unpack(cfr_data_hdr_fmt, binary_data1)
                cfr_data_hdr["magic_number"] =              data[0]
                cfr_data_hdr["vendor_id"] =                 data[1]
                cfr_data_hdr["CFR_header_version"] =        data[2]
                cfr_data_hdr["CFR_data_version"] =          data[3]
                cfr_data_hdr["chip_type"]                 = data[4]
                cfr_data_hdr["platform_type"] =             data[5]
                cfr_data_hdr["CFR_metadata_length"] =       data[6]
                cfr_data_hdr["host_real_ts"] =              data[7]
                cfr_data_hdr["capture_status"] =            data[8]
                cfr_data_hdr["capture_bandwidth"] =         data[9]
                cfr_data_hdr["channel_bandwidth"] =         data[10]
                cfr_data_hdr["phy_mode"] =                  data[11]
                cfr_data_hdr["prim20_channel"] =            data[12]
                cfr_data_hdr["band_center_freq1"] =         data[13]
                cfr_data_hdr["band_center_freq2"] =         data[14]
                cfr_data_hdr["capture_mode"] =              data[15]
                cfr_data_hdr["capture_type"] =              data[16]
                cfr_data_hdr["Space_Time_Streams_count"] =  data[17]
                cfr_data_hdr["RX_chain_mask_value"]      =  data[18]
                cfr_data_hdr["timestamp_CRF"]       =       data[19]
                cfr_data_hdr["CFR_dump_length"] =           data[20]
                cfr_data_hdr["is_MU_SU"] =                  data[21]
                cfr_data_hdr["num_mu_users"] =              data[22]
                cfr_data_hdr["peer_mac1"] =                 data[23:29]
                cfr_data_hdr["peer_mac2"] =                 data[29:35]
                cfr_data_hdr["peer_mac3"] =                 data[35:41]
                cfr_data_hdr["peer_mac4"] =                 data[41:47]
                cfr_data_hdr["channel_RSSI"] =              data[47:79]
                cfr_data_hdr["Phase_per_chain"] =           data[79:95]
                cfr_data_hdr["rtt_cfo_measurement"] =       data[95]
                cfr_data_hdr["agc_gain_per_chain"] =        data[96:105]
                cfr_data_hdr["rx_start_ts"] =               data[105]
                cfr_data_hdr["mcs_rate"] =                  data[106]
                cfr_data_hdr["gi_type"] =                   data[107]
                cfr_data_hdr["cfr_su_sig_info"] =           data[108:116]
                cfr_data_hdr["agc_gain_tbl_index"] =        data[116:124]
                
                #print(cfr_data_hdr)

                data_body_len = cfr_data_hdr["CFR_dump_length"]
                #print("got data body len:%d"%data_body_len)
                
                binary_data2 = recvall(conn, 48)
                if not binary_data2:
                    break
                data2 = struct.unpack(cfr_chip_hdr_fmt, binary_data2)

                cfr_chip_hdr_data["rtt_tag"] = get_middle_bits(data2[0], 0, 8)
                cfr_chip_hdr_data["header_length"] = get_middle_bits(data2[0], 8, 6)

                cfr_chip_hdr_data["upload_done"] = get_middle_bits(data2[1], 0, 1)             # 1bit
                cfr_chip_hdr_data["capture_type"] = get_middle_bits(data2[1], 1, 3)     # 3bits
                cfr_chip_hdr_data["preamble_type"] = get_middle_bits(data2[1], 4, 2)    # 2bits
                cfr_chip_hdr_data["nss"] = get_middle_bits(data2[1], 6, 3)              # 3bits
                cfr_chip_hdr_data["num_chains"] = get_middle_bits(data2[1], 9, 3)       # 3bits
                cfr_chip_hdr_data["upload_pkt_bw"] = get_middle_bits(data2[1], 12, 3)   # 3bits
                cfr_chip_hdr_data["sw_peer_id_valid"] = get_middle_bits(data2[1], 15, 1)# 1bit

                cfr_chip_hdr_data["sw_peer_id"] = data2[2]

                cfr_chip_hdr_data["phy_ppdu_id"] = data2[3]

                cfr_chip_hdr_data["total_bytes"] = data2[4]

                cfr_chip_hdr_data["header_version"] = get_middle_bits(data2[5], 0, 4)        # 4bits
                cfr_chip_hdr_data["target_id"] = get_middle_bits(data2[5], 4, 4)     # 4bits
                cfr_chip_hdr_data["cfr_fmt"] = get_middle_bits(data2[5], 8, 1)      # 1bit
                cfr_chip_hdr_data["mu_rx_data_incl"] = get_middle_bits(data2[5], 9, 1) # 1bit
                cfr_chip_hdr_data["freeze_data_incl"] = get_middle_bits(data2[5], 10, 1) # 1bit

                cfr_chip_hdr_data["mu_rx_num_users"] = get_middle_bits(data2[6], 0, 8)
                cfr_chip_hdr_data["decimation_factor"] = get_middle_bits(data2[6], 8, 4)

                #print(cfr_chip_hdr_data)
                    
                data_body_len -= 48 # skip chip header
                data_body_len += 4 # add tail mark
                
                binary_data3 = recvall(conn, data_body_len)
                if not binary_data2:
                    break
                
                iq_data = {
                'prim20_freq': cfr_data_hdr["prim20_channel"],
                'center_freq': cfr_data_hdr["band_center_freq1"],
                'num_streams': cfr_chip_hdr_data["nss"] + 1,
                'num_chains': cfr_chip_hdr_data["num_chains"],
                'upload_pkt_bw': cfr_chip_hdr_data["upload_pkt_bw"],
                'iq_d': binary_data3[:-4],
                'tag': sequence
                }

                #print(iq_data["iq_d"])
                if iq_data["prim20_freq"] != last_prim20_freq:
                    print("prim20 freq:%d"%iq_data["prim20_freq"])
                    last_prim20_freq = iq_data["prim20_freq"]
                
                if iq_data["center_freq"] != last_center_freq:
                    print("center freq:%d"%iq_data["center_freq"])
                    last_center_freq = iq_data["center_freq"]
                
                sequence += 1
                
                if sequence > 1000:
                    s.close()
                    break

                iq_dataout = parse_iq_data(iq_data)
                amp_data = iq_to_amplitude_phase(iq_dataout)
                data_queue.put(amp_data)
                #print(amp_data)

                print("got data header len:%d, body len:%d, seq:%d"%(cfr_data_hdr_len, data_body_len, sequence))

thread = threading.Thread(target=tcp_listener, daemon=True)
thread.start()

# 更新函数，为动画每一帧调用
def update(frame):
    global data_matrix
    if not data_queue.empty():
        new_data = data_queue.get()
        # 滚动数据矩阵，实现FIFO更新
        data_matrix[:, :-1] = data_matrix[:, 1:]
        data_matrix[:, -1] = new_data  # 插入最新数据至最后一列
        pcm.set_array(np.ravel(data_matrix))
    return pcm,

# 创建动画
ani = FuncAnimation(fig, update, interval=10, blit=True)

plt.show()
