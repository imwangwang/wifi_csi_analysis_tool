import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.widgets import Button
import argparse
import matplotlib.gridspec as gridspec
from file_parser import *

is_paused = False
should_exit = False

total_numof_signals = 1
total_numof_samples = 1000

csi_file_parser = None

num_freq = 54
buffer_length = 600  # Circular buffer length
sample_rate = 10  # 10 samples per second

def pause(event):
    global is_paused
    global csi_file_parser
    is_paused = not is_paused
    if csi_file_parser:
        csi_file_parser.pause()

def on_close(event):
    global should_exit
    should_exit = True
    print("User requested exit, closing...")

fig = plt.figure(figsize=(12, 18))
fig.canvas.mpl_connect('close_event', on_close)
gs = gridspec.GridSpec(2, 1)
ax1 = fig.add_subplot(gs[0, 0])
ax2 = fig.add_subplot(gs[1, 0])

pause_button_ax = plt.axes([0.8, 0.05, 0.1, 0.035])
pause_button = Button(pause_button_ax, 'Pause/Resume')
pause_button.on_clicked(pause)

time_ = np.linspace(0, buffer_length / sample_rate, buffer_length)
freq = np.linspace(1, 54, num_freq)

# Setting vertical offsets
amplitude_offset = 500  # Adjust this based on your amplitude range
phase_offset = 3.0  # Smaller offset for phase, since it's already within -pi to pi

# Initialize line plots for each frequency
line_objects_amp = [ax1.plot(time_, np.zeros(buffer_length), label=f'Freq {f}')[0] for f in range(num_freq)]
line_objects_phase = [ax2.plot(time_, np.zeros(buffer_length), label=f'Freq {f}')[0] for f in range(num_freq)]

ax1.set_xlim(0, buffer_length / sample_rate)
ax1.set_ylim(0, 1500 + num_freq * amplitude_offset)  # Adjust this based on expected amplitude range
ax1.set_xlabel('Time (seconds)')
ax1.set_ylabel('Amplitude')
ax1.set_title('Dynamic Line Plot of WiFi CSI Amplitude Data')

ax2.set_xlim(0, buffer_length / sample_rate)
ax2.set_ylim(-1.0, 1.0 + num_freq * phase_offset)  # Phase should be between -pi and pi
ax2.set_xlabel('Time (seconds)')
ax2.set_ylabel('Phase')
ax2.set_title('Dynamic Line Plot of WiFi CSI Phase Data')

def update(frame, parser):
    global is_paused, should_exit
    if should_exit:
        print("stopping animation.")
        plt.close()
        return line_objects_amp + line_objects_phase
    if is_paused:
        return line_objects_amp + line_objects_phase

    data = parser.get_data_from_queue()
    if data:
        new_amp_data, new_phase_data, diff_amplitudes, diff_phases, diff_amplitude_average, diff_phase_average, moving_detection = data
        # Shift data to the left and append new data at the end, applying offset
        for i, (line, new_data) in enumerate(zip(line_objects_amp, diff_amplitudes)):
            y_data = line.get_ydata()
            line.set_ydata(np.append(y_data[1:], new_data + i * amplitude_offset))
        for i, (line, new_data) in enumerate(zip(line_objects_phase, diff_phases)):
            y_data = line.get_ydata()
            line.set_ydata(np.append(y_data[1:], new_data + i * phase_offset))
    return line_objects_amp + line_objects_phase

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process offline captured CSI dump.")
    parser.add_argument('input', type=str, help="Input CSI dump file name")
    args = parser.parse_args()

    csi_file_parser = CSIFileParser(args.input)
    csi_file_parser.start()

    ani = FuncAnimation(fig, func=lambda frame: update(frame, csi_file_parser), interval=100, frames=1000, blit=False)
    plt.show()

    csi_file_parser.stop()
