import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# 假设数据
time = np.linspace(0, 1, 100)  # 时间从0到1秒
frequencies = np.linspace(1, 100, 30)  # 子载波频率从1Hz到100Hz
time_grid, freq_grid = np.meshgrid(time, frequencies, indexing='ij')

# 生成振幅和相位数据
amplitude = np.sin(2 * np.pi * freq_grid * time_grid)  # 振幅数据模拟
phase = np.cos(2 * np.pi * freq_grid * time_grid)  # 相位数据模拟

# 创建3D图
fig = plt.figure(figsize=(10, 8))
ax = fig.add_subplot(111, projection='3d')

# 绘制3D曲面图
surf = ax.plot_surface(time_grid, freq_grid, amplitude, facecolors=plt.cm.viridis((phase-np.min(phase))/(np.max(phase)-np.min(phase))))
cbar = plt.colorbar(surf, ax=ax, shrink=0.5, aspect=5)
cbar.set_label('Phase')

ax.set_xlabel('Time (s)')
ax.set_ylabel('Frequency (Hz)')
ax.set_zlabel('Amplitude')
ax.set_title('Amplitude with Phase Color Coding')

plt.show()
