import numpy as np
import matplotlib.pyplot as plt

# 假设数据
time = np.linspace(0, 1, 100)  # 时间从0到1秒
frequencies = np.linspace(1, 100, 30)  # 子载波频率从1Hz到100Hz
time_grid, freq_grid = np.meshgrid(time, frequencies, indexing='ij')

# 生成振幅和相位数据
amplitude = np.sin(2 * np.pi * freq_grid * time_grid)  # 振幅数据模拟
phase = np.cos(2 * np.pi * freq_grid * time_grid)  # 相位数据模拟

# 绘制振幅热图
plt.figure(figsize=(12, 6))
plt.subplot(1, 2, 1)
plt.contourf(time_grid, freq_grid, amplitude, levels=50, cmap='viridis')
plt.colorbar(label='Amplitude')
plt.xlabel('Time (s)')
plt.ylabel('Frequency (Hz)')
plt.title('Amplitude Heatmap')

# 绘制相位热图
plt.subplot(1, 2, 2)
plt.contourf(time_grid, freq_grid, phase, levels=50, cmap='viridis')
plt.colorbar(label='Phase')
plt.xlabel('Time (s)')
plt.ylabel('Frequency (Hz)')
plt.title('Phase Heatmap')

plt.tight_layout()
plt.show()
