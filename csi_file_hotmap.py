import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.colors import Normalize
from matplotlib.widgets import Button

import argparse
import matplotlib.gridspec as gridspec
from file_parser import *

is_paused = False
should_exit = False

total_numof_signals = 1
total_numof_samples = 1000

csi_file_parser = None

num_freq = 108
buffer_length = 600  # 环形缓冲区长度
sample_rate = 10  # 每秒10个样本

def pause(event):
    global is_paused
    global csi_file_parser
    is_paused = not is_paused
    if csi_file_parser:
        csi_file_parser.pause()

def on_close(event):
    global should_exit
    should_exit = True
    print("User requested exit, closing...")

fig = plt.figure(figsize=(12, 18))

fig.canvas.mpl_connect('close_event', on_close)
gs = gridspec.GridSpec(4, 2, width_ratios=[1, 0.2], wspace=0.1)
ax1 = fig.add_subplot(gs[0, 0])
ax2 = fig.add_subplot(gs[1, 0], sharex=ax1)
ax3 = fig.add_subplot(gs[2, 0])
ax4 = fig.add_subplot(gs[3, 0])
#fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(12, 18))
#plt.subplots_adjust(bottom = 0.1)
pause_button_ax = plt.axes([0.8, 0.35, 0.1, 0.035])
pause_button = Button(pause_button_ax, 'Pause/Resume')
pause_button.on_clicked(pause)

norm_amplitude = Normalize(vmin=0, vmax=3000)  # 归一化范围
norm_phase = Normalize(vmin=-1, vmax=1)

time_ = np.linspace(0, buffer_length / sample_rate, buffer_length)
freq = np.linspace(1, 108, num_freq)

data_matrix_amplitude = np.zeros((num_freq, buffer_length))
data_matrix_phase = np.zeros((num_freq, buffer_length))
line_amplitude_list = np.zeros(buffer_length)
phase_diff_subcarrier = np.zeros(buffer_length)
moving_detection = np.zeros(buffer_length)

pcm1 = ax1.pcolormesh(time_, freq, data_matrix_amplitude, cmap='viridis', norm=norm_amplitude, shading='auto')
pcm2 = ax2.pcolormesh(time_, freq, data_matrix_phase, cmap='viridis', norm=norm_phase, shading='auto')

line_amplitude, = ax3.plot(range(len(line_amplitude_list)), line_amplitude_list, label='amplitude', color='black')
line1, = ax4.plot(range(len(phase_diff_subcarrier)), phase_diff_subcarrier, label='subcarrier average', color='blue')
line2, = ax4.plot(range(len(moving_detection)), moving_detection, label='moving detection', color='red')

ax1.set_xlabel('Time (seconds)')
ax1.set_ylabel('Frequency')
ax1.set_title('Dynamic Heatmap of WiFi CSI Amplitude Data')

ax2.set_xlabel('Time (seconds)')
ax2.set_ylabel('Frequency')
ax2.set_title('Dynamic Heatmap of WiFi CSI Phase Data')

ax1 = fig.add_subplot(gs[0, 1])
ax2 = fig.add_subplot(gs[1, 1])
fig.colorbar(pcm1, ax=ax1)
fig.colorbar(pcm2, ax=ax2)

ax3.set_xlabel('Time (0.1 seconds)')
ax3.set_ylabel('Amplitude Difference')
ax3.set_title('Amplitude average difference')
ax3.legend(handles=[line_amplitude], loc='upper right')

ax4.set_xlabel('Time (0.1 seconds)')
ax4.set_ylabel('Phase Difference')
ax4.set_title('Phase average difference')

#ax4.legend(handles=[line1, line2, line3], loc='upper right')
ax3.set_xlim(0, buffer_length)
ax3.set_ylim(0, 3000)

ax4.set_xlim(0, buffer_length)
ax4.set_ylim(-0.5, 0.5)

def update(frame, parser):
    global data_matrix_amplitude, data_matrix_phase, phase_diff_subcarrier
    global is_paused, should_exit
    if should_exit:
        print("stopping animation.")
        plt.close()
        return []
    if is_paused:
        return pcm1, pcm2, line_amplitude, line1, line2
    data = parser.get_data_from_queue()
    if data:
        new_amp_data, new_phase_data, diff_amplitudes, diff_phases, diff_amplitude_average, diff_phase_average, detection = data
        # 滚动数据矩阵，实现FIFO更新
        data_matrix_amplitude[:, :-1] = data_matrix_amplitude[:, 1:]
        data_matrix_amplitude[:, -1] = new_amp_data  # 插入最新数据至最后一列
        pcm1.set_array(np.ravel(data_matrix_amplitude[:, :]))

        data_matrix_phase[:, :-1] = data_matrix_phase[:, 1:]
        data_matrix_phase[:, -1] = new_phase_data
        pcm2.set_array(np.ravel(data_matrix_phase[:, :]))

        phase_diff_subcarrier[:-1] = phase_diff_subcarrier[1:]
        phase_diff_subcarrier[-1] = diff_phase_average
        line1.set_data(range(buffer_length), phase_diff_subcarrier)

        #print(f"plot detection:{detection}")
        moving_detection[:-1] = moving_detection[1:]
        moving_detection[-1] = detection
        line2.set_data(range(buffer_length), moving_detection)

        line_amplitude_list[:-1] = line_amplitude_list[1:]
        line_amplitude_list[-1] = diff_amplitude_average
        line_amplitude.set_data(range(buffer_length), line_amplitude_list)

    return pcm1, pcm2, line_amplitude, line1, line2

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process offline captured CSI dump.")
    parser.add_argument('input', type=str, help="Input CSI dump file name")
    args = parser.parse_args()

    csi_file_parser = CSIFileParser(args.input)
    csi_file_parser.start()

    ani = FuncAnimation(fig, func=lambda frame: update(frame, csi_file_parser), interval=10, frames=1000, blit=True)
    plt.show()

    csi_file_parser.stop()